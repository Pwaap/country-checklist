import * as firebase from 'firebase/app';
import 'firebase/database';
import { mapStyles }from './maps.utils';

// Initialize Firebase
const firebaseConfig = {
  apiKey: "AIzaSyDARXbcZ-KVQIbWrqD8igXLSIst4Sdr750",
  authDomain: "my-countries-b559a.firebaseapp.com",
  databaseURL: "https://my-countries-b559a.firebaseio.com",
  projectId: "my-countries-b559a",
  storageBucket: "my-countries-b559a.appspot.com",
  messagingSenderId: "365345533199"
};
firebase.initializeApp(firebaseConfig);
// Get locations from Firebase
// Set locations
let locations = [];
let mapMarkers = [];
firebase.database().ref('/places').on('value', function(snapshot) {
  locations = [];
  snapshot.forEach(function(child){
    locations.push(child.val());
  });
  getStats();
  setMarkers();
});

// Initiate google maps
const map = new google.maps.Map(
  document.getElementById('map'), {
  center: {lat: 0, lng: 0},
  zoom: 3,
  disableDefaultUI: true,
  styles: mapStyles,
  disableDoubleClickZoom: true,
  scrollwheel:true
  }
)

// Add card to map
const card = document.getElementById('pac-card');
map.controls[google.maps.ControlPosition.TOP_LEFT].push(card);

let state = {
  place: {
    id: null,
    location: {
      lat: null,
      lng: null
    },
    address: null
  },
  name: null,
};

let button;


const infowindow = new google.maps.InfoWindow();
const infowindowContent = document.getElementById('infowindow-content');
infowindow.setContent(infowindowContent);
infowindowContent.children['add'].textContent = 'Add';
infowindowContent.children['add'].addEventListener('click', function(){
  addNewPlace();
});
infowindowContent.children['remove'].textContent = 'Remove';
infowindowContent.children['remove'].addEventListener('click', function(){
  removePlace();
});


// Initialize autocomplate from places API
const input = document.getElementById('pac-input');
const autocomplete = new google.maps.places.Autocomplete(input, {
  types: ['(cities)'],
});

autocomplete.bindTo('bounds', map);
autocomplete.setFields(['place_id', 'geometry', 'address_components', 'name']);
autocomplete.addListener('place_changed', function() {
  // Action when autocomplete option is selected
  infowindow.close();
  const newPlace = autocomplete.getPlace();
  state.place = {
    id: newPlace.place_id,
    location: {
      lat: newPlace.geometry.location.lat(),
      lng: newPlace.geometry.location.lng()
    },
    address: newPlace.address_components
  };
  state.name = newPlace.name,
  button = 'add';

  if (!newPlace.geometry) {
    return;
  } else {
    setInfoWindow();
    map.setCenter(state.place.location);
    map.setZoom(9);
  }
});

const icon = {
  url: '../../marker.png',
  scaledSize: new google.maps.Size(16, 24), // scaled size
  origin: new google.maps.Point(0, 0),
  anchor: new google.maps.Point(8, 24)
}

function setMarkers(){
  // Remove all locations
  if ( mapMarkers.length ){
    mapMarkers.forEach( (marker) => {
      marker.setMap(null);
    });
  };
  // Clear marker array
  mapMarkers = [];

  locations.forEach( (marker) => {
    const newMarker = new google.maps.Marker({
      map: map,
      icon: icon,
      anchorPoint: new google.maps.Point(0, -29),
      title: marker.place.id
    })
    newMarker.setPosition(new google.maps.LatLng(marker.place.location.lat, marker.place.location.lng));
    newMarker.addListener('click', function(){
      const id = this.title;
      showMarkerInfoWindow(id);
    });
    mapMarkers.push(newMarker);
  });

}

// Set infowindow
function setInfoWindow(){
  if ( button === 'add'){
    infowindowContent.children['remove'].style.display = "none";
    infowindowContent.children['add'].style.display = "block";
  } else if ( button === 'remove'){
    infowindowContent.children['add'].style.display = "none";
    infowindowContent.children['remove'].style.display = "block";
  };
  infowindowContent.children['place-name'].textContent = state.name;
  infowindow.setPosition(state.place.location);
  infowindow.open(map);
}

// Add new place...
function addNewPlace() {
  infowindow.close();
  // ...to app... and regenrate locations
  const item = locations.find(item => item.id === state.place.id);
  // Add only if it doesnt exist yet
  if ( !item ) {
    firebase.database().ref('places/' + state.place.id).set(state);
  }

}

// Remove place from Firebase
function removePlace(){
  infowindow.close();
  firebase.database().ref('places/' + state.place.id).remove();
}

// Show place info on marker click
function showMarkerInfoWindow(id){
  infowindow.close();

  const currentPlace = locations.find(item => item.place.id === id);
  state.place = {
    id: currentPlace.place.id,
    location: currentPlace.place.location,
    address: currentPlace.place.address
  };
  state.name = currentPlace.name,
  button = 'remove';

  setInfoWindow();
}

const statCities = document.getElementById('cities');
const statCountries = document.getElementById('countries');

function getStats(){
  // Cities
  statCities.textContent = locations.length;

  // Countries
  let uniqueCountries = [];
  locations.forEach( (location) => {
    location.place.address.filter( (item) => {
      if (item.types[0] === 'country') {
        if (!uniqueCountries.includes(item.long_name)) {
          uniqueCountries.push(item.long_name);
        }
      }
    });
  })

  statCountries.textContent = uniqueCountries.length;


}


